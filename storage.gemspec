# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'storage/version'

Gem::Specification.new do |spec|
  spec.name          = "storage"
  spec.version       = Storage::VERSION
  spec.authors       = ['Ilya Erin']
  spec.email         = ['Ilya.Erin@dataart.com']

  spec.summary       = %q{Storage gem, realized Trie algorithm.}
  spec.description   = %q{To use this gem added line "gem 'storage'" in your Gemfile.}
  spec.homepage      = ''

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''
  end

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
end
