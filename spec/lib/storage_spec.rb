require 'spec_helper'
require 'storage'
require 'fileutils'

RSpec.describe Storage::Storage do

  context 'base functional' do

    it 'by default should create empty trie' do
      storage = Storage::Storage.new
      expect(storage.to_s).to eq('')
    end

    context 'to_s should return trie as list, joins by ","' do
      it 'empty trie should return empty string' do
        storage = Storage::Storage.new
        expect(storage.to_s).to eq('')
      end

      it 'should work if in trie one element' do
        storage = Storage::Storage.new 'abc'
        expect(storage.to_s).to eq('abc')
      end

      it 'should work if in trie many elements' do
        storage = Storage::Storage.new 'aaaaaddff,abc,abcdd'
        expect(storage.to_s).to eq('aaaaaddff,abc,abcdd')
      end
    end

  end

  context 'add method' do
    before(:example) do
      @storage = Storage::Storage.new ''
    end

    it 'should add element in trie' do
      @storage.add 'abc'
      expect(@storage.to_s).to eq('abc')
    end

    it 'should add list of element, divided by ","' do
      @storage.add 'abc,bc,adf,a'
      expect(@storage.to_s).to eq('a,abc,adf,bc')
    end

    it 'should create sub path final, if it was added' do
      @storage.add 'abc,bc,adf'
      @storage.add 'ab'
      expect(@storage.to_s).to eq('ab,abc,adf,bc')
    end

    it 'should don\'t add element if it exist in trie' do
      @storage.add 'abc'
      @storage.add 'abc'
      expect(@storage.to_s).to eq('abc')
    end

    it 'should raise error if parameter isn\'t String object' do
      expect{@storage.add 1}.to raise_error(ArgumentError)
    end

    it 'should raise error if parameter don\'t consist of a-z,' do
      expect{@storage.add 1}.to raise_error(ArgumentError)
    end
  end

  context 'contains? method' do
    before(:example) do
      @storage = Storage::Storage.new 'abc,abcde,abcc,xyzz,xy'
    end

    it 'should return true, if element contains in trie' do
      expect(@storage.contains? 'abc').to be true
    end

    it 'should return false, if element not contains in trie' do
      expect(@storage.contains? 'bad').to be false
    end

    it 'should return false, if element not final in trie' do
      expect(@storage.contains? 'ab').to be false
    end
  end

  context 'find method' do
    before(:example) do
      @storage = Storage::Storage.new 'abc,abcde,abcdeddeef,abcc,xyzz,xy'
    end

    it 'should return empty array, if element not found ' do
      expect(@storage.find 'bad').to match_array([])
    end

    it 'should return array of find elements of trie' do
      expect(@storage.find 'abc').to match_array(['abc', 'abcde', 'abcdeddeef', 'abcc'])
    end

    it 'should return array of one element, if element not have childrens' do
      expect(@storage.find 'xyzz').to match_array(['xyzz'])
    end

    it 'should raise error if parameter length less then 3' do
      expect{@storage.find 'as'}.to raise_error(ArgumentError)
    end
  end

  context 'works with unzip files' do
    it 'should load data from test file' do
      storage = Storage::Storage.new
      storage.load_from_file 'spec/factories/storage'
      expect(storage.to_s).to eq('abc,abd,abds,acd,af,xyz')
    end

    it 'should save to file storage data' do
      file = 'tmp/storage'
      string = 'abc,abd,abds,acd,af,xyz'
      storage = Storage::Storage.new string
      storage.safe_to_file file

      string_from_file = ''
      File.open(file, 'r') { |f| string_from_file = f.readline }
      expect(string_from_file).to eq(string)
      FileUtils.rm file, force: true
    end
  end

  context 'works with zip files' do
    it 'should load data from test.zip file' do
      storage = Storage::Storage.new
      storage.load_from_zip 'spec/factories/storage.zip'
      expect(storage.to_s).to eq('abc,abd,abds,acd,af,xyz')
    end

    it 'should save to file storage data' do
      file = 'tmp/storage.zip'
      string = 'abc,abd,abds,acd,af,xyz'
      storage = Storage::Storage.new string
      storage.safe_to_zip file

      other_storage = Storage::Storage.new
      other_storage.load_from_zip file
      expect(other_storage.to_s).to eq(string)
      FileUtils.rm file
    end
  end

end