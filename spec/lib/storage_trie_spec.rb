require 'spec_helper'
require 'storage/trie'

RSpec.describe Storage::Trie do

    it 'by default should create empty trie object' do
      trie = Storage::Trie.new
      expect(trie.key).to eq('')
      expect(trie.final).to be false
      expect(trie.items).to match_array([])
    end

    it 'should have method key' do
      trie = Storage::Trie.new 'a'
      expect(trie.key).to eq('a')
    end

    it 'shouldn\'t have method key=' do
      trie = Storage::Trie.new 'a'
      expect{trie.key = 'b'}.to raise_error(NoMethodError)
    end

    it 'should have method "final?", aliases of "final"' do
      trie = Storage::Trie.new 'a', true
      expect(trie.final?).to be true
      expect(trie.final).to be true
    end

    it 'should have method final=, changed value of final"' do
      trie = Storage::Trie.new 'a', true
      expect(trie.final?).to be true
      trie.final = false
      expect(trie.final?).to be false
    end

    describe 'return object methods' do

      before(:example) do
        @trie_a = Storage::Trie.new 'a', true
        @trie_ab = Storage::Trie.new 'b', true
        @trie_abc = Storage::Trie.new 'c', true
        @trie_abd = Storage::Trie.new 'd', true
        @trie_ab.items << @trie_abc
        @trie_ab.items << @trie_abd
        @trie_a.items << @trie_ab
      end

      it 'to_a should return deep array of all trie items, and sub items, and etc...' do
        expect(@trie_a.to_a).to match_array(['a', ['ab', ['abc'], ['abd']]])
      end

      it 'to_list should return list array of all trie items, and sub items, and etc...' do
        expect(@trie_a.to_list).to match_array(['a', 'ab', 'abc', 'abd'])
      end

      it 'to_list should return only final elements of trie' do
        @trie_a.final = false
        @trie_abc.final = false
        expect(@trie_a.to_list).to match_array(['ab', 'abd'])
      end

    end

end