require 'storage/version'
require 'storage/trie'
require 'zip'

module Storage

  class Storage

    def initialize(str = '')
      @trie = Trie.new
      add(str)
    end

    def add(string)
      raise ArgumentError unless string.class == String && /\A[a-z,]*\z/ =~ string

      string.split(',').each do |word|
        tmp = ''
        parent_tree = @trie
        word.each_char do |char|
          tmp << char
          trie = find_by_path(tmp)

          if trie.present?
            if word == tmp
              trie.final = true
            else
              parent_tree = trie
            end
          else
            parent_tree.items << Trie.new(char, word == tmp)
            parent_tree = parent_tree.items.last
          end
        end
      end

      true
    end

    def contains?(path)
      trie = find_by_path(path)
      trie.present? ? trie.final? : false
    end

    def find(path)
      raise ArgumentError if path.size <= 2
      trie = find_by_path(path)
      trie.present? ? trie.to_list(path[0...-1]) : []
    end

    def load_from_file(file)
      File.open(file, 'r') { |f| add(f.readline) }
    end

    def safe_to_file(file)
      File.open(file, 'w') { |f| f.write self }
    end

    def load_from_zip(file)
      temp_file = "tmp/storage_#{rand(100)}"
      Zip::File.open(file) do |zip_file|
        zip_file.glob('storage').first.extract(temp_file)
      end
      load_from_file temp_file
      File.delete temp_file
    end

    def safe_to_zip(file)
      string = self.to_s
      Zip::File.open(file, Zip::File::CREATE) do |zip_file|
        zip_file.get_output_stream('storage', 0777) { |f| f.write string }
      end
    end

    def to_s
      @trie.to_list.sort.join(',')
    end

    private

      def find_by_path(path, trie = @trie, level = 0)
        word = path[level, 1]
        trie.items.each do |item|
          if item.key == word
            return (path.size == level + 1) ? item : find_by_path(path, item, level + 1)
          end
        end
        Trie.new
      end

  end
end
