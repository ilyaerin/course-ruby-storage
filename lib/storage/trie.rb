module Storage

  class Trie

    attr_reader :key
    attr_accessor :final, :items

    def initialize(key = '', final = false)
      @key = key
      @final = final
      @items = []
    end

    def present?
      !key.empty?
    end

    alias_method :final?, :final

    def to_a(prefix = '')
      list = []
      list << "#{prefix}#{key}" if final?
      list + items.map { |item| item.to_a("#{prefix}#{key}") }
    end

    def to_list(prefix = '')
      list = []
      list << "#{prefix}#{key}" if final?
      items.each { |item| list += item.to_list("#{prefix}#{key}") }
      list
    end

  end

end